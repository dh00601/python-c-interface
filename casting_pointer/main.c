#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
typedef int (*fn_def)(void);


typedef double(*fn_def2)(double, double);

int test1()
{
    // function to call
    printf("Test\n");
}

double test2(double num1, double num2)
{
    printf("%f %f", num1, num2);
}

void convert_and_call(void *fnvptr_val)
{
    // convert into pointer
    fn_def fnptr = (fn_def)fnvptr_val;
    fnptr();    
}

void convert_and_call2(void *fnvptr_val, double num1, double num2)
{
    // convert into pointer
    fn_def2 fnptr = (fn_def2)fnvptr_val;
    fnptr(num1, num2);
}

int main()
{
    void* fnvptr = (void*)&test1;

    // // print adress 
    // printf("%p\n", &test1);

    // printf("\n\nchar try\n");
    // // To char
    // // cast into char array
    // char* a=(char*)fnvptr;

    // // print stuff
    // printf("%s\n", a);
    // printf("size: %ld\n",sizeof(a));

    // // cast into new void thing
    // void * fnvptr2 = (void*)a;
    // printf("%p\n", fnvptr2);

    // // // convert into pointer
    // convert_and_call(fnvptr2);

    void* fnvptr3_old = (void*)&test2;
    printf("%p size: %ld\n", fnvptr3_old, sizeof(fnvptr3_old));
    char* fnvptr3_char=(char*)fnvptr3_old;
    printf("%s has size %ld\n", fnvptr3_char, sizeof(fnvptr3_char));
    void * fnvptr3_new = (void*)fnvptr3_char;
    printf("%p\n", fnvptr3_new);

    double numval_1 = 6.5;
    double numval_2 = 5.0;
    convert_and_call2(fnvptr3_new, numval_1, numval_2);
}