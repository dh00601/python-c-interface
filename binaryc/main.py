from functions import autogen_C_logging_code, binary_c_log_code, binary_c_write_log_code

import textwrap

# generate logging lines
logging_line = autogen_C_logging_code(
    {
        'MY_STELLAR_DATA': ['model.time', 'star[0].mass'], 
        'my_sss2': ['model.time', 'star[1].mass']
    }
)

# Generate code around logging lines
created_code = binary_c_log_code(logging_line)

binary_c_write_log_code(created_code)



















