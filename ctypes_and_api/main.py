import ctypes
import os
import subprocess 

import demo # load library with the C-interfacing

from functions import loadLibraryFunction, buildLibrary

# Build shared library
buildLibrary()

# Loading shared library
libmean = ctypes.CDLL("libmean.so.1") # loads the shared library

# Get memory adress of function. mimicking a pointer
mem_mean_function = ctypes.cast(libmean.mean, ctypes.c_void_p).value
print("mem adress of mean function {}, type: {}".format(mem_mean_function, type(mem_mean_function)))

mem_product_function = ctypes.cast(libmean.product, ctypes.c_void_p).value
print("mem adress of product function {}, type: {}".format(mem_product_function, type(mem_product_function)))

mem_sum_function = ctypes.cast(libmean.sum, ctypes.c_void_p).value
print("mem adress of sum function {}, type: {}".format(mem_sum_function, type(mem_sum_function)))

print('\n\n')

result_mean = demo.run_function_via_pointer(mem_mean_function, 10, 2.0)
print("Result of mean: {}\n".format(result_mean))

result_product = demo.run_function_via_pointer(mem_product_function, 10, 2.0)
print("Result of product: {}\n".format(result_product))

result_sum = demo.run_function_via_pointer(mem_sum_function, 10, 2.0)
print("Result of sum: {}\n".format(result_sum))
