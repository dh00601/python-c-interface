#include <stdio.h>
#include <stdlib.h>
#include "demolib.h"

typedef double (*fn_def)(double, double);

double run_function_via_pointer(long int str_1, double number_1, double number_2)
{
    // Function that recieves a long int containing the memory adress of a function, and two values
    // the long int will be interpreted as a pointer, and the two values will be passed to this function
    // will work as long as the function of which the memory adress is given accepts two doubles and returns a double

    // convert into pointer
    fn_def fnptr = (fn_def)str_1;

    // print some stuff
    // printf("test for running function with mem addr %ld with arguments %f %f\n", str_1, number_1, number_2);
    // printf("size of function pointer %zu\n",sizeof(fnptr));
    // printf("%p size: %zu\n", fnptr, sizeof(fnptr));

    double result = fnptr(number_1, number_2);
    return result;
}