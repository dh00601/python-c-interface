#include "mean.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mean(double a, double b)
{
  printf("calculating mean of %f and %f\n", a, b);
  double mean = (a+b)/2.0;
  return mean;
}

double product(double a, double b)
{
    printf("Calculating product of %f and %f\n", a, b);
    double product = a * b;
    return product;
}

double sum(double a, double b)
{
    printf("Calculating sum of %f and %f\n", a, b);
    double sum = a + b;
    return sum;
}