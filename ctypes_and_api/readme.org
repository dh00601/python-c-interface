* Idea:
- In the main function we compile the functions mean.c and mean.h to a shared library, calling it libmean.so.1
- this library gets loaded into memory with ctypes.CDLL(<name lib>)
- the memory adress, and two floats get given to a c function that is bound with python through the python-c-api:
  - these are defined in demolib.c
  - these are connected with python using demomodule.c

- within that c function, the memory adress is made into the pointer to the function that is contained in the loaded shared library. 
- This pointer is then called, with the two numbers and returns an output



* usage:
in this directory: pip install . --upgrade

then run python main.py


* Some leads:
- https://solarianprogrammer.com/2019/07/18/python-using-c-cpp-libraries-ctypes/
- https://gist.github.com/elyezer/7099291
- https://www.geeksforgeeks.org/turning-a-function-pointer-to-callable/
- https://stackoverflow.com/questions/9420673/is-it-possible-to-compile-c-code-using-python
- https://documentation.help/Python-3.2.1/ctypes.html
- https://stackoverflow.com/questions/49635105/ctypes-get-the-actual-address-of-a-c-function
- https://blog.regehr.org/archives/1621
- https://www.oreilly.com/library/view/understanding-and-using/9781449344535/ch01.html