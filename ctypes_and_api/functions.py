import ctypes
import os
import subprocess 

def buildLibrary(): 
    # store the return code of the c program(return 0) 
    # and display the output 
    s = subprocess.check_call("bash compile_shared_lib.sh", shell=True) 
    if not s==0:
        print("return code:\n{}".format(s))

def loadLibraryFunction():
    # OLD

    # Building library
    buildLibrary()

    # Loading library
    libmean = ctypes.CDLL("libmean.so.1") # loads the shared library

    # #
    # Pointer 
    # print("Function pointer of the function printing the function pointer: {}".format(libmean.print_pointer_to_mean))

    # Memory adress 
    # libmean.print_pointer_to_mean()

    # Get memory adress of function. mimicking a pointer
    mem = ctypes.cast(libmean.mean, ctypes.c_void_p).value

    # print some stuff
    # print("pointer to mean function (via python code): {}".format(mem))
    # print("Hex memory adress: {}".format(hex(mem)))
    # print(type(hex(mem)))

    # Other uses of this code
    # print(libmean.mean)
    # print(libmean.mean(ctypes.c_double(10), ctypes.c_double(3))) # call mean function needs cast arg types
    # libmean.mean.argtypes = [ctypes.c_double, ctypes.c_double] # define arguments types
    # print(libmean.mean(10, 3)) # call mean function does not need cast arg types
    # print(type(libmean.mean(10, 5)))

    return mem