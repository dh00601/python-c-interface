// demomodule.c
#include <Python.h>
#include "demolib.h"

// Declare the function wrapping the python call and the c function
static PyObject *DemoLib_RunFunctionViaPointer(PyObject *self, PyObject *args) {
    long int str_1;
    double number_1;
    double number_2;
    if (!PyArg_ParseTuple(args, "ldd", &str_1, &number_1, &number_2)) {
        return NULL;
    }
    // printf("%p contents %ld\n", &str_1, str_1);
    
    double result;
    result = run_function_via_pointer(str_1, number_1, number_2);
    return Py_BuildValue("d", result);
}

// module's function table
static PyMethodDef DemoLib_FunctionsTable[] = {
    {
        "run_function_via_pointer", // name exposed to Python
        DemoLib_RunFunctionViaPointer, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "Calculate the mean of two values via a pointer function" // documentation
    }, 

    {
        NULL, NULL, 0, NULL
    }
};

// modules definition
static struct PyModuleDef DemoLib_Module = {
    PyModuleDef_HEAD_INIT,
    "demo",     // name of module exposed to Python
    "Demo Python wrapper for custom C extension library.", // module documentation
    -1,
    DemoLib_FunctionsTable
};

PyMODINIT_FUNC PyInit_demo(void) {
    return PyModule_Create(&DemoLib_Module);
}