// demomodule.c
#include <Python.h>
#include "demolib.h"


// https://stackoverflow.com/questions/51505391/how-to-parse-void-to-pyobject


static PyObject *DemoLib_AddNumber(PyObject *self, PyObject *args) {
    int *number_1;
    int *number_2;
    if (!PyArg_ParseTuple(args, "ii", &number_1, &number_2)) {
        return NULL;
    }
    
    int result;
    result = add_number(number_1, number_2);

    return Py_BuildValue("i", result);
}

static PyObject *DemoLib_MakeStr(PyObject *self, PyObject *args) {
    char *character_1;
    char *character_2;
    if (!PyArg_ParseTuple(args, "ss", &character_1, &character_2)) {
        return NULL;
    }
    
    char* result;
    result = make_str(character_1, character_2);

    return Py_BuildValue("s", result);
}


static PyObject *DemoLib_AddNumberWithPointer(PyObject *self, PyObject *args) {
    int *number_1;
    int *number_2;
    if (!PyArg_ParseTuple(args, "ii", &number_1, &number_2)) {
        return NULL;
    }
    
    int result;
    int (*pf) (int, int);
    pf = &add_number;
    result = add_number_using_pointer(pf, number_1, number_2);
    return Py_BuildValue("i", result);
}










// module's function table
static PyMethodDef DemoLib_FunctionsTable[] = {
    {
        "add_number", // name exposed to Python
        DemoLib_AddNumber, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "adds two numbers" // documentation
    }, 


    {
        "add_number_with_pointer", // name exposed to Python
        DemoLib_AddNumberWithPointer, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "adds two numbers via a pointer function" // documentation
    }, 


    {
        "make_str", // name exposed to Python
        DemoLib_MakeStr, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "adds two strings" // documentation
    }, 


    {
        NULL, NULL, 0, NULL
    }
};






// modules definition
static struct PyModuleDef DemoLib_Module = {
    PyModuleDef_HEAD_INIT,
    "demo",     // name of module exposed to Python
    "Demo Python wrapper for custom C extension library.", // module documentation
    -1,
    DemoLib_FunctionsTable
};

PyMODINIT_FUNC PyInit_demo(void) {
    return PyModule_Create(&DemoLib_Module);
}