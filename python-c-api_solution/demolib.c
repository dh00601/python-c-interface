#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "demolib.h"

int add_number(int number_1, int number_2) {
    int result = number_1 + number_2;
    return result;
}

int multiply_number(int number_1, int number_2) {
    int result = number_1 * number_2;
    return result;
}


// https://stackoverflow.com/questions/8465006/how-do-i-concatenate-two-strings-in-c

char* make_str(const char *s1, const char *s2) {
    char *result = malloc(strlen(s1) + strlen(s2) + 1);
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

int add_number_using_pointer(int (*pf)(int, int), int number_1, int number_2) {
    int result;
    result = pf(number_1, number_2);
    return result;
}