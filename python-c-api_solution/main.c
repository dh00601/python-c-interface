#include <stdio.h>
#include <stdlib.h>
#include "demolib.h"

int main(void) {
    // Testing stuff for add_number function
    int getal_1=2;
    int getal_2=3;

    int result;
    result = add_number(getal_1, getal_2);
    printf("result of adding numbers: %d\n", result);
    //


    // testing stuff for concatenate string function
    char* str_1 = "a";
    char* str_2 = "b";

    char* s = make_str(str_1, str_2);
    // do things with s
    printf("result of concatting strings: %s\n", s);

    free(s); // deallocate the string
    // 

    // testing stuff to use a pointer to use a function
    int (*pf)(int, int);
    int pointer_res;

    pf = &add_number;
    pointer_res = (pf)(5, 9);
    printf("result of using a pointer to the add_number function: %d\n", pointer_res);
    // 

    // testing stuff to pass a pointer to a function that consecutively uses that pointer to add stuff
    int (*pf2)(int, int);
    int function_via_pointer_res;

    pf2 = &add_number;
    function_via_pointer_res = add_number_using_pointer(pf2, 10, 20);
    printf("result of calling a function with a function pointer to add_number: %d\n", function_via_pointer_res);

    int (*pf3)(int, int);
    int function_via_pointer_res_2;

    pf3 = &multiply_number;
    function_via_pointer_res_2 = add_number_using_pointer(pf3, 10, 20);
    printf("result of calling a function with a function pointer to multiply_number: %d\n", function_via_pointer_res_2);

    //
    return(0);
}
