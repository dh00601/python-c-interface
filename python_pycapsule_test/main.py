import demo
import time

def is_capsule(o):
    t = type(o)
    return t.__module__ == 'builtins' and t.__name__ == 'PyCapsule'

# Make capsule
caps = demo.create_capsule()

print(dir(caps))

# Print content (twice, just to see if it persists)
demo.print_content_capsule(caps)
demo.print_content_capsule(caps)

demo.free_content_capsule(caps) # Free it
demo.print_content_capsule(caps)

del caps

