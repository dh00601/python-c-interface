// demomodule.c
#include <Python.h>
#include "demolib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// void capsule_cleanup(PyObject *capsule) {
//     double *content_capsule;
//     if (!(content_capsule = (double*) PyCapsule_GetPointer(capsule, "DOUBLE")))
//         return NULL;

//     // Free stuff
//     printf("Via other function: Before deleting: Pointer %p and value %f\n", content_capsule, *content_capsule);
//     PyMem_Free(content_capsule);
//     printf("Via other function: After deleting: Pointer %p and value %f\n", content_capsule, *content_capsule);

//     // Use your specific gc implementation in place of free if you have to
//     free(content_capsule);
// }

// https://stackoverflow.com/questions/51505391/how-to-parse-void-to-pyobject

typedef struct {
   int   book_id;
} Books;


////////////////////////
static PyObject *free_content_capsule(PyObject *self, PyObject *args)
{
    PyObject *pycapsule = NULL;
    if (!PyArg_ParseTuple(args, "O", &pycapsule))
        return NULL;

    // double *content_capsule;
    // if (!(content_capsule = (double*) PyCapsule_GetPointer(pycapsule, "DOUBLE")))
    //     return NULL;
    Books *content_capsule;
    if (!(content_capsule = (Books*) PyCapsule_GetPointer(pycapsule, "BOOKS")))
        return NULL;

    PyMem_Free(content_capsule);
    // free(content_capsule);

    return Py_BuildValue("");
}

static PyObject *print_content_capsule(PyObject *self, PyObject *args)
{
    PyObject *pycapsule = NULL;
    if (!PyArg_ParseTuple(args, "O", &pycapsule))
        return NULL;

    Books *content_capsule;
    if (!(content_capsule = (Books*) PyCapsule_GetPointer(pycapsule, "BOOKS")))
        return NULL;

    printf("Pointer %p and value %d\n", content_capsule, content_capsule->book_id);

    return Py_BuildValue("");
}

Books *create_book_pointer(void);
Books *create_book_pointer(void)
{
    Books *content_capsule = (Books *)malloc(sizeof(Books *));
    if (NULL == content_capsule)
        return NULL;

    content_capsule->book_id = 6.0;
    return content_capsule;
}

double * create_double_pointer(void);
double * create_double_pointer(void)
{
    double *content_capsule = (double *) malloc(sizeof(double *));
    if (NULL == content_capsule)
        return NULL;
    *content_capsule = 6.0;

    return content_capsule;
}

static PyObject *create_capsule(PyObject *self, PyObject *args)
{
    // double *content_capsule = create_double_pointer();
    Books *content_capsule = create_book_pointer();

    // printf("Pointer %p and value %d\n", content_capsule, *content_capsule;
    printf("Pointer %p and value %d\n", content_capsule, content_capsule->book_id);

    // PyObject * ret = PyCapsule_New(content_capsule, "DOUBLE", NULL);
    PyObject * ret = PyCapsule_New(content_capsule, "BOOKS", NULL);
    return ret;
}


///////////////////////////////////////////////////////////////////////////////////
// module's function table
static PyMethodDef DemoLib_FunctionsTable[] = {
    // Capsule items
    {
        "create_capsule", // name exposed to Python
        create_capsule, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "Create capsule object" // documentation
    }, 

    {
        "print_content_capsule", // name exposed to Python
        print_content_capsule, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "Opens the capsule and prints content" // documentation
    }, 

    {
        "free_content_capsule", // name exposed to Python
        free_content_capsule, // C wrapper function
        METH_VARARGS, // received variable args (but really just 1)
        "Free the capsule memory" // documentation
    }, 


    {
        NULL, NULL, 0, NULL
    }
};

///////////////////////////////////////////////////////////////////////////////////
// modules definition
static struct PyModuleDef DemoLib_Module = {
    PyModuleDef_HEAD_INIT,
    "demo",     // name of module exposed to Python
    "Demo Python wrapper for custom C extension library.", // module documentation
    -1,
    DemoLib_FunctionsTable
};

PyMODINIT_FUNC PyInit_demo(void) {
    return PyModule_Create(&DemoLib_Module);
}