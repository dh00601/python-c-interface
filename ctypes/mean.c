#include "mean.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mean(double a, double b) {
  printf("calculating mean with %f and %f", a, b);
  return (a+b)/2;
}

void print_pointer_to_mean() {
    printf("pointer to mean function (via c code): %p\n", &mean);
}

double calc_mean_using_pointer(double (*pf)(double, double), double number_1, double number_2) {
    double result;
    result = pf(number_1, number_2);
    printf("result running mean function with pointer to the function: %f", result);
    return result;
}