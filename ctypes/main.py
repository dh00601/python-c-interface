# https://solarianprogrammer.com/2019/07/18/python-using-c-cpp-libraries-ctypes/
# https://gist.github.com/elyezer/7099291

# https://www.geeksforgeeks.org/turning-a-function-pointer-to-callable/
# https://stackoverflow.com/questions/9420673/is-it-possible-to-compile-c-code-using-python
# https://documentation.help/Python-3.2.1/ctypes.html


# https://stackoverflow.com/questions/49635105/ctypes-get-the-actual-address-of-a-c-function


import ctypes
import os
import subprocess 
  
def buildLibrary(): 
  
    # store the return code of the c program(return 0) 
    # and display the output 
    s = subprocess.check_call("bash compile.sh", shell=True) 
    if not s==0:
        print("return code:\n{}".format(s))

def loadLibraryFunction():
    # Building library
    buildLibrary()

    # Loading library
    libmean = ctypes.CDLL("libmean.so.1") # loads the shared library

    # #
    # Pointer 
    # print("Function pointer of the function printing the function pointer: {}".format(libmean.print_pointer_to_mean))

    # Memory adress 
    # libmean.print_pointer_to_mean()

    # Get memory adress of function. mimicking a pointer
    mem = ctypes.cast(libmean.mean, ctypes.c_void_p).value

    # print some stuff
    # print("pointer to mean function (via python code): {}".format(mem))
    # print("Hex memory adress: {}".format(hex(mem)))
    # print(type(hex(mem)))

    return hex(mem)

hex_mem = loadLibraryFunction()
print(hex_mem)


# Other uses of this code
# print(libmean.mean)
# print(libmean.mean(ctypes.c_double(10), ctypes.c_double(3))) # call mean function needs cast arg types
# libmean.mean.argtypes = [ctypes.c_double, ctypes.c_double] # define arguments types
# print(libmean.mean(10, 3)) # call mean function does not need cast arg types
# print(type(libmean.mean(10, 5)))